#!/usr/bin/python3
from requests import get
from bs4 import BeautifulSoup
from random import shuffle
import json, heapq, threading, subprocess, time

class PlaylistQueue:
    def __init__(self):
        self.exitBool = False
        self._queue = []
        self._count = 0
        self.soundCommand = None
        self.position = 0
        self.soundTemplate = ['ffplay', '-loglevel', '8', '-hide_banner', '-autoexit', '-nodisp', 'null']
        threading.Thread(target=self.precache).start()

    def precache(self):
        while len(self._queue) == 0:
            time.sleep(0.1)
        link = heapq.heappop(self._queue)
        print(link)
        self.add(link[2], link[0]+255)
        self.cacheinfo = json.loads(subprocess.check_output(
          ['ffprobe', '-v', '-8', '-i', link[2], '-print_format', 'json', '-show_format', '-show_streams', '-hide_banner'], universal_newlines=True))
        self.soundCommand = self.soundTemplate.copy()
        self.soundCommand[6] = link[2]
        self.position+=1
        self.position=self.position%(len(self._queue)+1)

    def add(self, link, priority):
        heapq.heappush(self._queue, (priority, self._count, link))
        self._count+=1

    def playThread(self):
        soundCommand = None
        while not self.exitBool:
            while self.soundCommand == None or self.soundCommand == soundCommand:
                time.sleep(0.1)
            self.info = self.cacheinfo
            soundCommand = self.soundCommand.copy()
            threading.Thread(self.precache()).start()
            self.output = subprocess.Popen(soundCommand)
            self.output.wait()

def main():
    html = get("https://www.4chan.org/wsg/catalog").text
    jsScript = BeautifulSoup(html, 'html.parser').find_all('script')[2].text
    catalogVar = jsScript.split('var catalog =')[1].split('var style_group')[0][:-1]
    catalog = json.loads(catalogVar)['threads']
    playlist = PlaylistQueue()
    musicThreads = [elem for elem in catalog if ygylCheck(catalog[elem])]
    print('{} threads'.format(len(musicThreads)))
    threading.Thread(target=startThreads, args=(musicThreads, playlist, )).start()
    del html
    del jsScript
    del catalogVar
    del catalog
    try:
        playthread = threading.Thread(target=playlist.playThread)
        playthread.start()
        while not playlist.exitBool:
            test = input('type stuff: ')
            if test == 'skip':
                playlist.output.terminate()
            elif test == 'exit':
                raise KeyboardInterrupt
    except KeyboardInterrupt:
        playlist.exitBool = True
        playlist.output.terminate()
        playthread.join()
        exit()

def ygylCheck(threadAttr):
    checkList = ['ygyl', 'music', 'groove', 'amv']
    for x in checkList:
        if x in threadAttr['sub'].lower() or x in threadAttr['teaser'].lower(): return True
    return False
    
def startThreads(musicThreads, playlist):
    for x in musicThreads:
        threading.Thread(target=musicGrab, args=(x, playlist, ), daemon=True).start()
    
def musicGrab(thread, playlist):
    html = get("https://boards.4chan.org/wsg/thread/"+thread).text
    soup = BeautifulSoup(html, 'html.parser')
    webms = soup.select('.fileText a')
    shuffle(webms)
    def webmFormat(x):
        return 'http:'+x.get('href')
    webms = list(map(webmFormat, webms))
    webms = [elem for elem in webms if elem[-4:] == 'webm']
    for webm in webms:
        playlist.add(webm, webms.index(webm))
    
if __name__ == '__main__':
    main()
